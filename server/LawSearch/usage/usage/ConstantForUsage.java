package usage;

import java.nio.file.Path;
import java.nio.file.Paths;

public interface ConstantForUsage {
	// Path to the raw data (which should go into the index)
	public static Path PATHTODATA = Paths.get("C:/Users/Sven Landmann/Documents/Git/LawSearch/data");
	// Path to the index
	public static Path PATHTOINDEX = Paths.get("C:/Users/Sven Landmann/Documents/TestIndex");
	// Path to the stop word file
	public static Path PATHTOSTOPWORDLIST = Paths
			.get("C:/Users/Sven Landmann/Documents/Git/LawSearch/server/LawSearch/war/WEB-INF/conf/stop.txt");
	// Path where the random picked up laws should be saved
	public static Path PATHTOEVALUATIONLAWS = Paths.get("C:/Users/Sven Landmann/Desktop/Evaluation");
	// name of the corpus
	public static String CORPUS = "brd";
	// Path to the Evaluation<CORPUS>.xml w
	public static Path PATHTOEVALUATIONXML = Paths.get(
			"C:/Users/Sven Landmann/Documents/Git/LawSearch/Evaluation/semiAutomatischeEvaluation/",
			"Evaluation" + CORPUS.toUpperCase() + ".xml");
}
