package evaluation;

import java.time.LocalDate;

/**
 * Defines all tags used in the evaluation xml and provides methods to generate
 * xml tags
 * 
 * @author Sven Landmann
 *
 */
public enum XMLTagsEvaluation {
	SEARCH("search"), QUERY("query"), PURPOSE("purpose"), RESULT("result"), RELEVANTDOCUMENTS(
			"relevant"), NOTRELEVANTDOCUMENTS("notrelevant"), DOCNR("docnr"), EVALUATION(
					"evaluation"), LASTUPDATE("lastupdate"), DAY("day"), MONTH("month"), YEAR("year");

	private final String xmlTag;

	private XMLTagsEvaluation(String xmlTag) {
		this.xmlTag = xmlTag;
	}

	public String getOpenXmlTag() {
		return "<" + xmlTag + ">";
	}

	public String getCloseXmlTag() {
		return "</" + xmlTag + ">";
	}

	public String getXmlTag() {
		return xmlTag;
	}

	/**
	 *
	 * @return the current used DTD for the evaluation xml.
	 */
	public static String getDTD() {
		StringBuilder sb = new StringBuilder();
		sb.append("<!DOCTYPE ").append(EVALUATION.getXmlTag()).append("[\r\n");

		sb.append("<!ELEMENT ").append(EVALUATION.getXmlTag()).append(" (");
		sb.append(LASTUPDATE.getXmlTag()).append(", ");
		sb.append(SEARCH.getXmlTag()).append("*)>\r\n");

		sb.append("<!ELEMENT ").append(LASTUPDATE.getXmlTag()).append(" (");
		sb.append(DAY.getXmlTag()).append(", ");
		sb.append(MONTH.getXmlTag()).append(", ");
		sb.append(YEAR.getXmlTag()).append(")>\r\n");

		sb.append("<!ELEMENT ").append(DAY.getXmlTag()).append(" (#PCDATA) ").append(">\r\n");
		sb.append("<!ELEMENT ").append(MONTH.getXmlTag()).append(" (#PCDATA) ").append(">\r\n");
		sb.append("<!ELEMENT ").append(YEAR.getXmlTag()).append(" (#PCDATA) ").append(">\r\n");

		sb.append("<!ELEMENT ").append(SEARCH.getXmlTag()).append(" (");
		sb.append(QUERY.getXmlTag()).append(", ");
		sb.append(PURPOSE.getXmlTag()).append(", ");
		sb.append(RESULT.getXmlTag()).append(")>\r\n");

		sb.append("<!ELEMENT ").append(QUERY.getXmlTag()).append(" (#PCDATA) ").append(">\r\n");
		sb.append("<!ELEMENT ").append(PURPOSE.getXmlTag()).append(" (#PCDATA) ").append(">\r\n");

		sb.append("<!ELEMENT ").append(RESULT.getXmlTag()).append(" (");
		sb.append(RELEVANTDOCUMENTS.getXmlTag()).append("?, ");
		sb.append(NOTRELEVANTDOCUMENTS.getXmlTag()).append("?)>\r\n");

		sb.append("<!ELEMENT ").append(RELEVANTDOCUMENTS.getXmlTag()).append(" (");
		sb.append(DOCNR.getXmlTag()).append("*)>\r\n");

		sb.append("<!ELEMENT ").append(NOTRELEVANTDOCUMENTS.getXmlTag()).append(" (");
		sb.append(DOCNR.getXmlTag()).append("*)>\r\n");

		sb.append("<!ELEMENT ").append(DOCNR.getXmlTag()).append(" (#PCDATA) ").append(">\r\n");

		sb.append("]>\r\n");

		return sb.toString();
	}

	/**
	 *
	 * @return the intro for an xml file
	 */
	public static String getIntro() {
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";
	}

	/**
	 *
	 * @return the beginning of the xml tree
	 */
	public static String getBeginning() {
		StringBuilder sb = new StringBuilder();

		LocalDate date = java.time.LocalDate.now();

		sb.append(EVALUATION.getOpenXmlTag());
		sb.append(LASTUPDATE.getOpenXmlTag());

		sb.append(DAY.getOpenXmlTag());
		sb.append(date.getDayOfMonth());
		sb.append(DAY.getCloseXmlTag());

		sb.append(MONTH.getOpenXmlTag());
		sb.append(date.getMonth());
		sb.append(MONTH.getCloseXmlTag());

		sb.append(YEAR.getOpenXmlTag());
		sb.append(date.getYear());
		sb.append(YEAR.getCloseXmlTag());

		sb.append(LASTUPDATE.getCloseXmlTag());

		sb.append("\r\n");

		return sb.toString();
	}

	/**
	 *
	 * @return the end of xml file
	 */
	public static String getEnding() {
		return EVALUATION.getCloseXmlTag();
	}

}
