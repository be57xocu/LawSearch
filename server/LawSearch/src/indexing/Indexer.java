package indexing;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import org.apache.lucene.index.CorruptIndexException;

import generated.LawSearchAPI.Law;

public interface Indexer {

	/**
	 * Close all open streams
	 *
	 * @throws CorruptIndexException
	 * @throws IOException
	 */
	void close() throws CorruptIndexException, IOException;

	/**
	 * index a single Law
	 *
	 * @param law
	 * @throws IOException
	 */
	void indexLaw(Law law) throws IOException;

	/**
	 * index all given Laws
	 *
	 * @param laws
	 * @throws IOException
	 */
	void indexLaws(List<Law> laws) throws IOException;

	/**
	 * creates a property file for an index in war/WEB-INF/conf/
	 *
	 * @param corpusName
	 *            Name of the new corpus
	 * @param language
	 *            Language of the corpus files
	 * @param key
	 *            the unique key of this corpus
	 * @param stopWordlistFileName
	 *            name of the file containing all stop words
	 * @param indexPath
	 *            path of the index
	 * @throws IOException
	 */
	void createProperty(String corpusName, String language, String key, String stopWordlistFileName, Path indexPath)
			throws IOException;

}