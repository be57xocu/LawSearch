package indexing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import generated.LawSearchAPI.Law;

/**
 * Handler to convert the given xml files to laws.
 *
 * @author Sven Landmann
 *
 */
public class XML_Laws_BRD_ContentHandler extends DefaultHandler {
	private List<Law> laws;
	private Law.Builder law;
	private StringBuilder context;
	private StringBuilder headline;
	private StringBuilder shortname;
	private StringBuilder lawbook;
	private StringBuilder activeContent;

	private HashMap<String, Integer> tableValues;

	private String currentText;

	public XML_Laws_BRD_ContentHandler() {
		this.laws = new ArrayList<>();
		law = Law.newBuilder();

	}

	public XML_Laws_BRD_ContentHandler(int approximatCount) {
		this.laws = new ArrayList<>(approximatCount);
		law = Law.newBuilder();
	}

	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) {
		switch (qName.toLowerCase()) {
		case "norm":
			context = new StringBuilder();
			headline = new StringBuilder();
			shortname = new StringBuilder();
			lawbook = new StringBuilder();
			law.clearDocnb();
			law.clearLawbook();
			law.clearParagraph();
			break;
		case "fundstellen":
			break;
		case "periodikum":
			break;
		case "langue":
			activeContent = headline;
			break;
		case "textdaten":
			activeContent = context;
			break;
		case "p":
			if (activeContent != null)
				activeContent.append("<p>");
			break;
		case "dd":
			if (activeContent != null)
				activeContent.append("<dd>");
			break;
		case "dl":
			if (activeContent != null)
				activeContent.append("<dl>");
			break;
		case "dt":
			if (activeContent != null)
				activeContent.append("<dt>");
			break;
		case "br":
			if (activeContent != null)
				activeContent.append("<br>");
			break;
		case "table":
			if (activeContent != null)
				activeContent.append("<table>");
			tableValues = new HashMap<>();
			break;
		case "colspec":
			int colnum = Integer.parseInt(atts.getValue("colnum"));
			;
			tableValues.put(atts.getValue("colname"), colnum);
			break;
		case "attarea":
			break;
		case "fnarea":
			break;
		case "toc":
			break;
		case "revision":
			break;
		case "title":
			if (activeContent != null)
				activeContent.append("<h1>");
			break;
		case "subtitle":
			break;
		case "kommentar":
			break;
		case "b":
			if (activeContent != null)
				activeContent.append("<b>");
			break;
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		currentText = new String(ch, start, length);
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		switch (qName.toLowerCase()) {
		case "norm":
			break;
		case "jurabk":
		case "amtabk":
			shortname.append(currentText + " ");
			break;
		case "enbez":
			law.setParagraph(currentText);
			break;

		case "fundstellen":
			break;
		case "periodikum":
			law.setLawbook(currentText);
			break;
		case "langue":
			activeContent = null;
			break;
		case "textdaten":
			activeContent = null;
			break;
		case "p":
			if (activeContent != null)
				activeContent.append("</p>");
			break;
		case "dd":
			if (activeContent != null)
				activeContent.append("</dd>");
			break;
		case "dl":
			if (activeContent != null)
				activeContent.append("</dl>");
			break;
		case "dt":
			if (activeContent != null)
				activeContent.append("</dt>");
			break;

		case "b":
		case "table":
			if (activeContent != null)
				activeContent.append("</table>");
			break;
		case "AttArea":
			break;
		case "FnArea":
			break;
		case "TOC":
			break;
		case "Revision":
			break;
		case "Title":
			if (activeContent != null)
				activeContent.append("</h1>");
			break;
		case "Subtitle":
			break;
		case "Kommentar":
			break;
		}
	}

	public List<Law> getLaws() {
		return this.laws;
	}

}
