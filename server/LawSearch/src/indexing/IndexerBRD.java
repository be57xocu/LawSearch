package indexing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;

import analyzer.AnalyzerBRD;
import generated.LawSearchAPI.Law;
import lawsearch.DocumentConstants;
import server.Configuration;

/**
 * A class to create new indexes for LawSearch for laws of the BRD
 *
 * @author Sven Landmann
 *
 */
public class IndexerBRD implements Indexer {

	private IndexWriter writer;

	public IndexerBRD(Directory directory, BufferedReader pathToStopWords) throws IOException {

		AnalyzerBRD analyzer = new AnalyzerBRD();
		analyzer.init(pathToStopWords);
		IndexWriterConfig config = new IndexWriterConfig(analyzer);
		writer = new IndexWriter(directory, config);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see indexing.Indexer#close()
	 */
	@Override
	public void close() throws CorruptIndexException, IOException {
		writer.close();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see indexing.Indexer#indexLaw(generated.LawSearchAPI.Law)
	 */
	@Override
	public void indexLaw(Law law) throws IOException {
		Document doc = new Document();
		doc.add(new StringField(DocumentConstants.DOCNR, law.getDocnb(), Field.Store.YES));
		doc.add(new StringField(DocumentConstants.LAWBOOK, law.getLawbook(), Field.Store.YES));
		doc.add(new TextField(DocumentConstants.SHORTNAME, law.getShortname(), Field.Store.YES));
		doc.add(new TextField(DocumentConstants.HEADLINE, law.getHeadline(), Field.Store.YES));
		doc.add(new StringField(DocumentConstants.PARAGRAPH, law.getParagraph(), Field.Store.YES));
		doc.add(new TextField(DocumentConstants.CONTENT, law.getText(), Field.Store.YES));
		this.writer.addDocument(doc);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see indexing.Indexer#indexLaws(java.util.List)
	 */
	@Override
	public void indexLaws(List<Law> laws) throws IOException {
		for (Law law : laws) {
			indexLaw(law);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see indexing.Indexer#createProperty(java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.nio.file.Path)
	 */
	@Override
	public void createProperty(String corpusName, String language, String key, String stopWordlistFileName,
			Path indexPath) throws IOException {
		Path path = Paths.get("war/WEB-INF/conf/").toAbsolutePath();

		Configuration config = new Configuration();

		config.setProperty(Configuration.CORPUS_NAME, corpusName);
		config.setProperty(Configuration.CORPUS_LANGUAGE, language);
		config.setProperty(Configuration.CORPUS_REGISTRY_KEY, key);
		config.setProperty(Configuration.PATH, indexPath.toString());
		config.setProperty(Configuration.STOPWORDLIST, stopWordlistFileName);

		config.store(
				new BufferedWriter(
						new FileWriter(path.resolve("lawsearch_" + corpusName.toLowerCase() + ".property").toFile())),
				"");

	}

}
