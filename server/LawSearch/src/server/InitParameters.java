/**
 *
 */
package server;

/**
 * @author Sven Landmann
 *
 */
public final class InitParameters {

	public static final String CORPUS_DEFAULT_KEY = "corpus.default.key";
	public static final String CORPUS_CONFIGURATION_FILES = "corpus.configuration.file";
	public static final String CORPUS_NAME = "corpus.name";
	public static final String CORPUS_REGISTRY_KEY = "corpus.registry.key";
	public static final String CORPUS_LANGUAGE = "corpus.language";
	public static final String PATH = "path";
}