/**
 *
 */
package server;

import java.util.Properties;

/**
 * @author Sven Landmann
 *
 */
public class Configuration extends Properties {

	private static final long serialVersionUID = 1L;

	public static final String CORPUS_NAME = "corpus.name";
	public static final String CORPUS_REGISTRY_KEY = "corpus.registry.key";
	public static final String CORPUS_LANGUAGE = "corpus.language";
	public static final String PATH = "corpus.path";
	public static final String STOPWORDLIST = "corpus.stopwords";

	public static final String[] KEYS = { PATH, CORPUS_NAME, CORPUS_REGISTRY_KEY, CORPUS_LANGUAGE, STOPWORDLIST };
}
