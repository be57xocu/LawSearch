package analyzer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.StopFilter;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.Tokenizer;

/**
 * Defines the Analyzer for indexing the laws of the BRD. Includes a stop word
 * filter, a stammer and a lowerCaseFilter.
 *
 * Needs to be initialize via init!
 *
 * @author Sven Landmann
 *
 */
public class AnalyzerBRD extends Analyzer {
	private CharArraySet stopWordSet;

	public AnalyzerBRD() {
		super();
	}

	@Override
	protected TokenStreamComponents createComponents(String fieldname) {
		Tokenizer tokenizer = new XMLTokenizer();

		TokenFilter filter = new StopFilter(tokenizer, stopWordSet);
		filter = new GermanStemmarFilter(filter);
		filter = new LowerCaseFilter(filter);

		return new TokenStreamComponents(tokenizer, filter);
	}

	/**
	 * reads a file and extract all first words from each line and adds them the the
	 * stop words set.
	 *
	 * @param pathToStopWords
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void init(BufferedReader pathToStopWords) throws FileNotFoundException, IOException {
		List<String> stopWords = new ArrayList<>();

		while (pathToStopWords.ready()) {
			String line = pathToStopWords.readLine();

			if (line.startsWith(" ") || line.isEmpty())
				continue;

			String word[] = line.split(" ", 2);
			stopWords.add(word[0]);
		}
		stopWordSet = StopFilter.makeStopSet(stopWords, true);
	}

	/**
	 * @return the stop word set if this class was initialized via init;
	 */
	public CharArraySet getStopWordSet() {
		return this.stopWordSet;
	}

}
