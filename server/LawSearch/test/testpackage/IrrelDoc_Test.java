package testpackage;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.w3c.dom.*;
import org.xml.sax.*;
import javax.xml.parsers.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.*;

public class IrrelDoc_Test {

	public IrrelDoc_Test() {
		// TODO Auto-generated constructor stub
	}
	
	public static String nodeToString (Node node) throws TransformerException {
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        StreamResult result = new StreamResult(new StringWriter());
        DOMSource source = new DOMSource(node);
        transformer.transform(source, result);
        String xmlString = result.getWriter().toString();
        return xmlString;
	}
	
	public static Document xmlStringToDocu(String xmlString) throws SAXException, IOException, ParserConfigurationException {
		Document docu = DocumentBuilderFactory.newInstance()
                .newDocumentBuilder()
                .parse(new InputSource(new StringReader(xmlString)));
        docu.getDocumentElement().normalize();
        return docu;
	}
	
	public static NodeList  extractNodeFromDocu( XPath xPath,Document docu, String nodename) throws XPathExpressionException {
		NodeList paraList = (NodeList) xPath.compile(nodename).evaluate(
                docu, XPathConstants.NODESET); 
		return paraList;
	}
	
	public static ArrayList<File> listFilesInFolder(File folder){
		ArrayList<File> files=new ArrayList<File>();
		for(File fileEntry:folder.listFiles()) {
			if(fileEntry.isDirectory()) {
				listFilesInFolder(fileEntry);
			}else {
				files.add(fileEntry);
			 }
		}
		return files;
	}
	
	
	public  static void main(String[] arg) throws IOException, ParserConfigurationException, 
	SAXException, XPathExpressionException, TransformerFactoryConfigurationError, TransformerException {
		//this example show that, it works
		File file=new File("/home/bing/Downloads/IR/Irrel_Test/BJNR000010935.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        dBuilder = dbFactory.newDocumentBuilder();
        XPath xPath =  XPathFactory.newInstance().newXPath();
        Document doc = dBuilder.parse(file);
        doc.getDocumentElement().normalize();

        String expression = "/dokumente/norm";	
        NodeList normList = (NodeList) xPath.compile(expression).evaluate(
                doc, XPathConstants.NODESET);  
        
        for (int i = 0; i < normList.getLength(); i++) {
            Node norm = normList.item(i);
            System.out.println("\nCurrent Element :" + norm.getNodeName());
            
            // whether it is a ELEMent node
            if (norm.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) norm;
                System.out.println("DokumentNr :" + eElement.getAttribute("doknr"));    
            }else {System.out.println("it is not a element node!");}
            
            //Node to String transform
            String xmlString = nodeToString(norm);
           
            //transform String(xml form) to document
            Document docu=xmlStringToDocu(xmlString);	
            
            // choose node from Document
            NodeList paraList = (NodeList) extractNodeFromDocu(xPath,docu,"//P");
            
            //whether all the paragraph is null  or has ChildNode(not include <I>)
            int numbIrrePara=0;
            
            //if the norm has no P(paragraph Element) 
        	if(paraList.getLength()!=0) {
        		// if all Paragraph have subNode
        		for(int j=0;j<paraList.getLength();j++) {
        			Node para= paraList.item(j);
        			System.out.println(nodeToString(para));
        			System.out.println("this paragraph has Child"+para.hasChildNodes()+"\n");
        			NodeList paraChildren=para.getChildNodes();
        			Boolean isDirectory=false;
        			for(int z=0;z<paraChildren.getLength();z++) {
        				System.out.println(paraChildren.item(z).getNodeName());
        				if(paraChildren.item(z).getNodeName()!="#text" && paraChildren.item(z).getNodeName()!="I" ) {
        					isDirectory=true;
        				}       				
        				/*System.out.println(paraChildren.item(z).getNodeType());
        				if(paraChildren.item(z).getNodeType()!=3) {
        					isText=false;
        				}*/
        			}
        			if(isDirectory==true||para.hasChildNodes()==false) {
        				numbIrrePara++;
        			}
        		}
        		if(numbIrrePara==paraList.getLength()) {
        			if (norm.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) norm;
                        System.out.println(" will be deleted DokumentNr :" + eElement.getAttribute("doknr"));    
                    }
        			
        			norm.getParentNode().removeChild(norm);
        		}
        		
        	}else {
        		if (norm.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) norm;
                    System.out.println(" will be deleted DokumentNr :" + eElement.getAttribute("doknr"));    
                }
        		norm.getParentNode().removeChild(norm);
        	 }
            
           	String newDoc=nodeToString(doc);
        	System.out.println(newDoc);
                          
        }		
	}

}
